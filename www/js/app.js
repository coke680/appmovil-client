// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'satellizer'])

.constant('API_URL', 'http://app-movil.dev/api/')

.run(['$ionicPlatform', '$rootScope', '$state', function($ionicPlatform, $rootScope, $state ) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

  $rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
          var isLogin = toState.name === 'login';

          var user = JSON.parse(localStorage.getItem('user'));
          var report = JSON.parse(localStorage.getItem('report'));
          $rootScope.currentUser = user;
          $rootScope.report = report;
          $rootScope.authenticated = true;

          if(!window.localStorage.getItem('authenticate') && !isLogin)
          {
            $state.go('login');
          }
  });
}])

.config(function(API_URL, $stateProvider, $urlRouterProvider, $authProvider, $httpProvider, $provide, $ionicConfigProvider) {

    function redirectWhenLoggedOut($q, $injector) {

      return {

          responseError: function(rejection) {

              // Need to use $injector.get to bring in $state or else we get
              // a circular dependency error
              var $state = $injector.get('$state');

              // Instead of checking for a status code of 400 which might be used
              // for other reasons in Laravel, we check for the specific rejection
              // reasons to tell us if we need to redirect to the login state
              var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];

              // Loop through each rejection reason and redirect to the login
              // state if one is encountered
              angular.forEach(rejectionReasons, function(value, key) {

                  if(rejection.data.error === value) {

                      // If we get a rejection corresponding to one of the reasons
                      // in our array, we know we need to authenticate the user so
                      // we can remove the current user from local storage
                      localStorage.removeItem('user');
                      // Send the user to the auth state so they can login
                      $state.go('login');
                  }
              });

              return $q.reject(rejection);
          }
      };
  }

  $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);

  // Push the new factory onto the $http interceptor array
  $httpProvider.interceptors.push('redirectWhenLoggedOut');

  $authProvider.loginUrl = API_URL + 'authenticate';

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  .state('login', {
      cache: false,
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl' 
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'menuContent': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.account', {
    cache: false,
    url: '/account',
    views: {
      'menuContent': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});
