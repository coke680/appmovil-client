angular.module('starter.controllers', [])

.controller('LoginCtrl', function(API_URL, $ionicLoading, $scope, $http, $auth, $rootScope, $state, $ionicHistory, $ionicPopup){

    $scope.data = {};
    $scope.button = true;

    $scope.login = function() {

    	  $ionicLoading.show({
            template: '<ion-spinner icon="android"></ion-spinner>'
        });

        var credentials = {
            client_number: $scope.data.client_number,
            password: $scope.data.password
        };

        $scope.button = false;

         $auth.login(credentials).then(function(data) {
            $http.get('http://app-movil.dev/api/authenticate/user').then(function(response){

                var user = JSON.stringify(response.data.user);
                localStorage.setItem('user', user);

                var status = JSON.stringify(response.data.status);
                localStorage.setItem('status', status);

                $rootScope.authenticated = true;
                localStorage.setItem('authenticated', true);

                $rootScope.currentUser = response.data.user;
                $rootScope.status = response.data.status;


                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                $scope.button = true;
                $ionicLoading.hide();
                $state.go('tab.dash', {});
            });

        }, function () {
        		    $ionicLoading.hide();
              	$scope.button = true;
              	var alertPopup = $ionicPopup.alert({
                    title: '<i class="ion-close-circled"></i> Error',
                    template: 'Numero inexistente o contraseña incorrectos.',
                    buttons: [
                      {text: 'Volver a intentar', type: 'button-positive'}
                    ]
              });
        });
    };
})

.controller('DashCtrl', function($scope, $http, $ionicPopup, $rootScope, $ionicLoading,) {
    $scope.a = {};

    if($rootScope.status == 0 || $rootScope.status == null){
        $scope.a.notification = false;
    }
    else {
        $scope.a.notification = true;
    }

    $scope.change = function() {
    
      var confirmPopup = $ionicPopup.confirm({
        title: 'Confirmar',
        template: '<p class="text-center">¿Confirmar acción?</p>'

        }).then(function(res) {

          $ionicLoading.show({
              template: '<ion-spinner icon="android"></ion-spinner>'
          });

          if(res) {
              var status = '';

              if($scope.a.notification){
                  status = 1;
              }
              else {
                  status = 0;
              }

              $http({
                  url: 'http://app-movil.dev/activations',
                  method: "POST",
                  data: { 'status' : status }
              }).then(function(response) {

              if($scope.a.notification){
                  $ionicLoading.hide();
                  var alertPopup = $ionicPopup.alert({
                        title: 'Información enviada',
                        template: '<p class="text-center"><i class="icon ion-log-out"></i> ¡Monitoreo activado!</p>'
                    });
                }

                else {
                  $ionicLoading.hide();
                  var alertPopup = $ionicPopup.alert({
                        title: 'Información enviada',
                        template: '<p class="text-center"><i class="icon ion-log-out"></i> ¡Monitoreo Desactivado!</p>'
                    });
                }             
              }, 
              function(response) {
                  $ionicLoading.hide();
                  var alertPopup = $ionicPopup.alert({
                        title: 'Error al enviar información',
                        template: '<p class="text-center"><i class="icon ion-log-out"></i> Intente nuevamente</p>'
                    });
                    if($scope.a.notification){
                    $scope.a.notification = false;
                  }
                  else {
                    $scope.a.notification = true;
                  }
              });
          }
          else {
              if($scope.a.notification){
                  $scope.a.notification = false;
              }
              else {
                  $scope.a.notification = true;
              }
              $ionicLoading.hide();
          }
      });

    }
})

.controller('AccountCtrl', function($scope, $rootScope, $ionicLoading, $auth, $http, $ionicPopup ) {

    $ionicLoading.show({
        template: '<ion-spinner icon="android"></ion-spinner>'
    });

    $http.get('http://app-movil.dev/api/authenticate/report').then(function(response){

        var report = JSON.stringify(response.data.report);
        localStorage.setItem('report', report);

        $rootScope.authenticated = true;
        localStorage.setItem('authenticated', true);

        $rootScope.report = response.data.report;
        $ionicLoading.hide();         

    }); 
})

.controller('LogoutCtrl', function($scope, $rootScope, $auth, $location, $ionicHistory, $timeout, $state) {
    $scope.logout = function() {
        $auth.logout().then(function() {
            localStorage.removeItem('user');
            localStorage.removeItem('status');
            localStorage.removeItem('authenticated');
            $rootScope.authenticated = false;
            $rootScope.currentUser = null;
            $timeout(function () {
                $ionicHistory.clearCache();
                $ionicHistory.clearHistory();
                $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
                $state.go('login');
            }, 30);
        });
      };
})

;

